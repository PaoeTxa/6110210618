#pragma checksum "D:\6110210618\NewsReport\Pages\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5c42addff3717c60d467cbea14d7729edb374adb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(NewsReport.Pages.Pages_Index), @"mvc.1.0.razor-page", @"/Pages/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/Index.cshtml", typeof(NewsReport.Pages.Pages_Index), null)]
namespace NewsReport.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\6110210618\NewsReport\Pages\_ViewImports.cshtml"
using NewsReport;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5c42addff3717c60d467cbea14d7729edb374adb", @"/Pages/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"88c01d45ccfb24904f468bbbfdaedb0a8afbac7d", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(26, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "D:\6110210618\NewsReport\Pages\Index.cshtml"
  
    ViewData["Title"] = "ระบบรายงานข่าว";

#line default
#line hidden
            BeginContext(78, 347, true);
            WriteLiteral(@"
<div class=""jumbotron"">
 <center><h2>ระบบรายงานข่าวโดยประชาชนเพื่อประชาชน</h2><center>
</div>

<div class=""row"">
    <h3> ข่าวทั้งหมดในระบบ </h3>
    <table class=""table"">
    <thead>
        <tr>
            <th>หมวดข่าว</th><th>วันที่เกิดเหตุ</th><th>เนื้อข่าว</th><th>สถานะการยืนยัน</th>
        </tr>
    </thead>

    <tbody>
");
            EndContext();
#line 22 "D:\6110210618\NewsReport\Pages\Index.cshtml"
     foreach (var item in Model.NewsCategory) {

#line default
#line hidden
            BeginContext(474, 45, true);
            WriteLiteral("        <div class = \"row\">\r\n            <h3>");
            EndContext();
            BeginContext(520, 39, false);
#line 24 "D:\6110210618\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(model =>item.ShortName));

#line default
#line hidden
            EndContext();
            BeginContext(559, 23, true);
            WriteLiteral("</h3>\r\n            <h4>");
            EndContext();
            BeginContext(583, 38, false);
#line 25 "D:\6110210618\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(model =>item.FullName));

#line default
#line hidden
            EndContext();
            BeginContext(621, 23, true);
            WriteLiteral("</h4>\r\n        </div>\r\n");
            EndContext();
#line 27 "D:\6110210618\NewsReport\Pages\Index.cshtml"
        
    }

#line default
#line hidden
            BeginContext(661, 32, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
